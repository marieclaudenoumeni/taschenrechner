package com.example.calculate_app

import android.R
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

lateinit  var  textView:TextView
lateinit var button: Button
lateinit var button0:Button
lateinit var button1:Button
lateinit var button2:Button
lateinit var button3:Button
lateinit var button4:Button
lateinit var button5:Button
lateinit var button6:Button
lateinit var button7:Button
lateinit var button8:Button
lateinit var button9:Button
lateinit var buttonclear:Button
lateinit var buttongleich:Button
lateinit var buttonplus:Button
lateinit var buttonminus:Button
lateinit var buttonmal:Button
lateinit var buttongeteilt:Button
lateinit var buttonwurzel:Button
lateinit var buttonpotenz:Button
lateinit var buttonE:Button
lateinit var buttonsinus:Button
lateinit var buttoncosinus:Button
lateinit var buttonarcsinus:Button
lateinit var buttonarccosinus:Button
lateinit var buttonrezi:Button
lateinit var buttonpi:Button
lateinit var buttonKomma:Button
lateinit var buttonEuler:Button
lateinit var buttonln:Button
lateinit var buttone:Button
lateinit var buttonprozent:Button
lateinit var br: Berechnung


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button = findViewById(R.id.button1)
        textView = findViewById(R.id.text1)


   fun  guiElementeRegistrieren() {
        // textView =  findViewById(R.id.text1)
       // br = new Berechnung();
        button1=  findViewById(R.id.button1)
        button.setOnClickListener{
                textView.setText(textView.getText()+ "1")
        }
        button2 = findViewById(R.id.button2);
        button2.setOnClickListener {
                textView.setText(textView.getText()+ "2")
            }

        button3 = findViewById(R.id.button3);
        button3.setOnClickListener{
                textView.setText(textView.getText()+ "3")
            }
       button4 =findViewById(R.id.button4);
        button4.setOnClickListener {
                textView.setText(textView.getText()+ "4")
        }
        button5 = findViewById(R.id.button5)
        button5.setOnClickListener{
                textView.setText(textView.getText()+ "5")
        }
        button6 = findViewById(R.id.button6);
        button6.setOnClickListener {
                textView.setText(textView.getText()+ "6");
        }
         button7 = findViewById(R.id.button7)
        button7.setOnClickListener{
                textView.setText(textView.getText()+ "7")
        }
         button8 = findViewById(R.id.button8);
        button8.setOnClickListener {
                textView.setText(textView.getText()+ "8")
        }
        button9 = findViewById(R.id.button9);
        button9.setOnClickListener{
                textView.setText(textView.getText()+ "9")
        }
        button0 =  findViewById(R.id.button0);
        button0.setOnClickListener{
            fun onClick(v:View ) {
                textView.setText(textView.getText()+ "0")
            }
        }
         buttonKomma = findViewById(R.id.buttonKomma)
        buttonKomma.setOnClickListener{
                //Ein Komma wird nur benötigt, wenn noch keines in der Anzeige vorhanden ist
                if (!textView.getText().toString().contains(".")) {
                    textView.setText(textView.getText() + ".")
                }
        }
        buttonclear =  findViewById(R.id.btnClear)
        buttonclear.setOnClickListener{
                textView.setText("");
                br.setZahl1("0");
                br.setZahl2("0");
        }
         buttongleich =  findViewById(R.id.buttongleich)
        buttongleich.setOnClickListener{
    //'Gleich' soll nur ausgeführt werden, wenn Das Lösungsfeld nicht leer ist, es nicht nur ein Komma enthält oder nicht mit "Er" (wie "Error") beginnt.
                if(!textView.getText().toString().contentEquals("")&&!textView.getText().
                    toString().contentEquals(".")&&!textView.getText().toString().
                    startsWith("Er")) {
                    br.setZahl2(textView.getText().toString())
                    textView.setText(br.rechne());
                }
            }
         buttonplus = findViewById(R.id.buttonplus)
        buttonplus.setOnClickListener {
                zweistelligeVerknüpfung("+");
        }
        buttonminus = findViewById(R.id.buttonminus)
        buttonminus.setOnClickListener{
    //Wenn das Lösungsfeld leer ist, ist das – ein Vorzeichenminus ansonsten das Rechenminus
                    if (textView.getText().toString().equals("")) {
                        textView.setText("-")
                    }
                    else {
                        zweistelligeVerknüpfung("-")
                    }
            }
       buttonmal = findViewById(R.id.buttonmal)
        buttonmal.setOnClickListener{
                zweistelligeVerknüpfung("*")
            }
        buttongeteilt =  findViewById(R.id.buttongeteilt)
        buttongeteilt.setOnClickListener{
                zweistelligeVerknüpfung(":")
        }
         buttonwurzel =  findViewById(R.id.buttonwurzel)
        buttonwurzel.setOnClickListener{
                    einstelligeVerknüpfung("w")
        }
        buttonpotenz =  findViewById(R.id.buttonpotenz);
        buttonpotenz.setOnClickListener {
                einstelligeVerknüpfung("p");
        }
        buttonwurzel = findViewById(R.id.buttonwurzel)
        buttonwurzel.setOnClickListener{
                zweistelligeVerknüpfung("nw")
        }
        buttonpotenz = findViewById(R.id.buttonpotenz)
        buttonpotenz.setOnClickListener{
                zweistelligeVerknüpfung("np")
        }
         buttonln = findViewById(R.id.buttonLn)
        buttonln.setOnClickListener{
       einstelligeVerknüpfung("ln")
        }
         buttone =  findViewById(R.id.buttone);
        buttone.setOnClickListener{
                einstelligeVerknüpfung("e")
        }
         buttonsinus = findViewById(R.id.buttonsinus)
        buttonsinus.setOnClickListener {
                einstelligeVerknüpfung("sin")
        }
         buttoncosinus = (findViewById(R.id.buttoncosinus)
        buttoncosinus.setOnClickListener{
                einstelligeVerknüpfung("cos")
        }
        buttonarcsinus = findViewById(R.id.buttonarcsinus)
        buttonarcsinus.setOnClickListener{
                einstelligeVerknüpfung("asin")
        }
        buttonarccosinus =  findViewById(R.id.buttonarccosinus)
        buttonarccosinus.setOnClickListener{
                einstelligeVerknüpfung("acos")
        }
         buttonprozent =  findViewById(R.id.buttonprozent)
        buttonprozent.setOnClickListener{
                zweistelligeVerknüpfung("%")
        }
        buttonrezi = (ButtonfindViewById(R.id.buttonrezi)
        buttonrezi.setOnClickListener{
                einstelligeVerknüpfung("/")
        }
         buttonpi = findViewById(R.id.buttonpi)
        buttonpi.setOnClickListener{
                //Ein Pi soll nur hinzugefügt werden, wenn das Lösungsfeld leer ist
                if (textView.getText().toString().equals("")) {
                    textView.setText(textView.getText() + String.valueOf(Math.PI))
                }
        }
         buttonEuler =  findViewById(R.id.buttonEuler)
        buttonEuler.setOnClickListener{
                //Die Zahl e soll nur hinzugefügt werden, wenn das Lösungsfeld leer ist
                if (textView.getText().toString().equals("")) {
                    textView.setText(textView.getText() + String.valueOf(Math.E))
                }
            }
    }

fun zweistelligeVerknüpfung(op: String?)
{
//}Dies soll nur ausgeführt werden, wenn Das Lösungsfeld nicht leer ist, es nicht nur ein Komma enthält oder nicht mit "Er" (wie "Error") beginnt.

    if(!textView.getText().toString().contentEquals("")&&
        !textView.getText().toString().contentEquals(".")&&
        !textView.getText().toString().startsWith("Er"))
    {
        br.setZahl1(textView.getText().toString())
        br.setOperation(op)
        textView.setText("")
    }

fun einstelligeVerknüpfung(op: String?) {
    //Dies soll nur ausgeführt werden, wenn Das Lösungsfeld nicht leer ist, es nicht nur ein Komma enthält oder nicht mit "Er" (wie "Error") beginnt.
    if (!textView.getText().toString().contentEquals("") &&
        !textView.getText().toString().contentEquals(".") &&
        !textView.getText().toString().startsWith("Er")
    ) {
        br.setZahl1(textView.getText().toString())
        br.setOperation(op)
        textView.setText(br.rechne())
    }
}
}
    }
}


fun onCreateOptionsMenu(menu: Menu): Boolean{

}

fun onOptionsittemSelected(menuitem: MenuItem):Boolean{

}