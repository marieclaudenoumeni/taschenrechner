package com.example.calculate_app

import java.util.*

class Sortieren {
    private val zahlen: IntArray
    private fun erzeugeZufallszahlen() {
        val zufallsZahl = Random()
        for (i in zahlen.indices) {
            zahlen[i] = zufallsZahl.nextInt(98) + 1
        }
    }

    private fun sortiereArray(nr: IntArray) {
        val zahl = nr.size
        for (wieviel in 1 until zahl) {
            for (ubrigeZahlen in 0 until zahl - wieviel) {
                if (nr[ubrigeZahlen] > nr[ubrigeZahlen + 1]) {
                    val temp = nr[ubrigeZahlen]
                    nr[ubrigeZahlen] = nr[ubrigeZahlen + 1]
                    nr[ubrigeZahlen + 1] = temp
                }
            }
        }
        println(zahlen[zahl]) //System.out.println(zahlen[i])
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Sortieren()
        }
    }

    init {
        zahlen = IntArray(20)
        erzeugeZufallszahlen()
        sortiereArray(zahlen) //   sortiereArray(int[])
    }
}
