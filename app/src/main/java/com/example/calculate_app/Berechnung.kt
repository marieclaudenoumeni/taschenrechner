package com.example.calculate_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Berechnung{


    var zahl1: Double = 0.0
    var zahl2: Double= 0.0
    var operation: String = ""

    override fun toString(): String {
        return super.toString()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Berechnung

        if (zahl1 != other.zahl1) return false
        if (zahl2 != other.zahl2) return false
        if (operation != other.operation) return false

        return true
    }

    override fun hashCode(): Int {
        var result = zahl1.hashCode()
        result = 31 * result + zahl2.hashCode()
        result = 31 * result + operation.hashCode()
        return result
    }

    override fun setZahl1(z: Double) {
        this.zahl1 = z
    }

   override fun getZahl1(): Double {
        return zahl1
    }

    override fun setZahl2(z: Double) {
        this.zahl2 = z
    }

    override  fun getZahl2():Double {
        return zahl2

    }

    override  fun getOperation():String {
        return operation
    }

    override fun setOperation(toString: String){
        this.operation = toString
    }


    fun rechne() :String {
        if (this.getOperation().equals("+")) {
            return String.valueOf(this.getZahl1() + this.getZahl2());
        } else if (this.getOperation().equals("-")) {
            return String.valueOf(this.getZahl1() - this.getZahl2());
        } else if (this.getOperation().equals("*")) {
            return String.valueOf(this.getZahl1() * this.getZahl2());
        } else if (this.getOperation().equals(":")) {
            if (this.getZahl2() != 0.0) {
                return String.valueOf(this.getZahl1() / this.getZahl2());
            } else {
                return "Error";
            }
        } else if (this.getOperation().equals("w")) {
            if (this.getZahl1() >= 0) {

                return String.valueOf(Math.sqrt(this.getZahl1()));
            } else {
                return "Error";
            }
        }

        else if (this.getOperation().equals("p")) {
            return String.valueOf(Math.pow(this.getZahl1(), 2));
        } else if (this.getOperation().equals("nw")) {
            return String.valueOf(Math.pow(this.getZahl2(), 1 / this.getZahl1()));
        } else if (this.getOperation().equals("np")) {
            return String.valueOf(Math.pow(this.getZahl1(), this.getZahl2()));
        } else if (this.getOperation().equals("ln")) {
            if (this.getZahl1() > 0) {
                return String.valueOf(Math.log(this.getZahl1()));
            } else {
                return "Error";
            }
        } else if (this.getOperation().equals("e")) {
            return String.valueOf(Math.pow(Math.E, this.getZahl1()));
        } else if (this.getOperation().equals("sin")) {
            return String.valueOf(Math.sin(this.getZahl1()));
        } else if (this.getOperation().equals("cos")) {
            return String.valueOf(Math.cos(this.getZahl1()));
        } else if (this.getOperation().equals("asin")) {
            return String.valueOf(Math.asin(this.getZahl1()));
        } else if (this.getOperation().equals("acos")) {
            return String.valueOf(Math.acos(this.getZahl1()));
        } else if (this.getOperation().equals("%")) {

            return String.valueOf(this.getZahl1() * this.getZahl2() / 100);
        } else if (this.getOperation().equals("/")) {
            if (this.getZahl1() != 0.0) {
                return String.valueOf(1 / this.getZahl1());
            } else {
                return "Error";
            }
        } else {
            return "Error";
        }
    }


}

